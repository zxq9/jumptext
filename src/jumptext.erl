-module(jumptext).
-vsn("0.1.1").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").
-export([j/2, j/3,
         lang_defs/0, lang_id_lookup/1, lang_label_lookup/1,
         lang_id/1, lang_label/1, script_priority/1,
         add_l10n_menu/1, lang_buttons/2,
         init_dict/1, init_mod/1, lookup/3,
         katakana/0, hiragana/0, kana/0, latin/0, numeric/0,
         check_script/1, check_script/2, coerce/2]).
-export_type([lang/0, lang_id/0, lang_def/0, label/0, transdata/0, transdict/0]).


%%%  The j/2,3 functions accept a {lang(), transdict()} argument. This
%%%  argument is typically labeled J throughout the project, and the helper
%%%  functions make_j/1 and j/2 are the idiomatic way the J value and j/2,3
%%%  functions are accessed within translation-dependent modules. This is
%%%  analogous to the gettext idiom of _("string") and Qt's tr("string"):
%%%    J = make_j(State),
%%%    Translation = j(J, value_reference),


-opaque lang() :: en_US
                | ja_JP.
-opaque lang_id() :: 4000..4299.
-opaque lang_def() :: {lang(), lang_id(), unicode:chardata()}.
-opaque label() :: atom().
-type def() :: {lang(), unicode:chardata()}.
-type trans() :: {def(), [def()]}.
-opaque transdata() :: [{label(), trans()}].
-opaque transdict() :: dict:dict(label(), trans()).
-type script() :: latin | kanji | hiragana | katakana | kana.


%%% Functions imported by calling code

-spec j({lang(), transdict()}, label()) -> unicode:chardata().

j(J, L) ->
    j(J, L, []).


-spec j({lang(), transdict()}, label(), list()) -> unicode:chardata().

j({Lang, Dict}, Label, Subs) ->
    case dict:find(Label, Dict) of
        {ok, {{_, Default}, Trans}} ->
            Format = proplists:get_value(Lang, Trans, Default),
            io_lib:format(Format, Subs);
        error ->
            io_lib:format(atom_to_list(Label) ++ " + ~tp", [Subs])
    end.


%% Static definitions

-spec lang_defs() -> [lang_def()].
%% Wx reserves ID from 4999 to 5999; Aka will never require anywhere near 4000.

lang_defs() ->
    [{en_US, 4000, "en_US.png", "English"},
     {ja_JP, 4001, "ja_JP.png", "日本語"}].

-spec lang_id_lookup(lang_id()) -> lang_def() | false.

lang_id_lookup(LangId) -> lists:keyfind(LangId, 2, lang_defs()).


-spec lang_label_lookup(lang()) -> lang_def() | false.

lang_label_lookup(Lang) -> lists:keyfind(Lang, 1, lang_defs()).


-spec lang_id(lang_def()) -> lang_id().

lang_id({_, Id, _}) -> Id.


-spec lang_label(lang_def()) -> lang().

lang_label({Label, _, _}) -> Label.


-spec script_priority(lang()) -> [script()].

script_priority(en_US) -> [latin, katakana, kanji, hiragana];
script_priority(ja_JP) -> [kanji, katakana, hiragana, latin].


add_l10n_menu(MenuBar) ->
    L10n = wxMenu:new(),
    Items = [wxMenuItem:new([{id, I}, {text, N}]) || {_, I, _, N} <- lang_defs()],
    Add = fun(Item) -> wxMenu:append(L10n, Item) end,
    ok = lists:foreach(Add, Items),
    wxMenuBar:append(MenuBar, L10n, "&Language"),
    ok.


lang_buttons(Parent, IconDir) ->
    Bitmap = fun(I) -> wxBitmap:new(filename:join(IconDir, I)) end,
    Langs = [{ID, Bitmap(Icon)} || {_, ID, Icon, _} <- lang_defs()],
    [wxBitmapButton:new(Parent, Lang, Icon) || {Lang, Icon} <- Langs].


-spec init_dict(TransData) -> TransDict
    when TransData :: transdata() | empty,
         TransDict :: transdict().

init_dict(empty)     -> dict:new();
init_dict(TransData) -> dict:from_list(TransData).


-spec init_mod(module()) -> transdict().

init_mod(Mod) ->
    {ok, AkaDir} = aka_control:get_conf(aka_dir),
    {ok, Theme} = aka_control:get_conf(theme, "standard"),
    TransFile = atom_to_list(Mod) ++ ".trans",
    Path = filename:join([AkaDir, "theme", Theme, "l10n", TransFile]),
    case file:consult(Path) of
        {ok, Props} -> init_dict(proplists:get_value(translations, Props, empty));
        {error, _}  -> init_dict(empty)
    end.


-spec lookup(label(), lang(), transdict()) -> {lang(), unicode:chardata()}.

lookup(Label, Lang, Dict) ->
    case dict:find(Label, Dict) of
        {ok, {Default, Trans}} -> lookup(proplists:lookup(Lang, Trans), Default);
        error                  -> {no_trans, atom_to_list(Label)}
    end.

-spec lookup(Result, Default) -> def()
    when Result  :: def() | none,
         Default :: def().

lookup(none, Default) -> Default;
lookup(Result, _)     -> Result.


katakana() ->
    {ok, MP} =
        re:compile("^[アイウエオ"
                     "カキクケコ"
                     "サシスセソ"
                     "タチツテト"
                     "ナニヌネノ"
                     "ハヒフヘホ"
                     "マミムメモ"
                     "ヤユヨ"
                     "ラリルレロ"
                     "ワヲン"
                     "ァィゥェォ"
                     "ャュョッ"
                     "ガギグゲゴ"
                     "ザジズゼゾ"
                     "ダヂヅデド"
                     "バビブベボ"
                     "パピプペポ"
                     " 　ー]*$",
                   [unicode]),
    MP.


hiragana() ->
    {ok, MP} =
        re:compile("^[あいうえお"
                     "かきくけこ"
                     "さしすせそ"
                     "たちつてと"
                     "なにぬねの"
                     "はひふへほ"
                     "まみむめも"
                     "やゆよ"
                     "らりるれろ"
                     "わをん"
                     "ぁぃぅぇぉ"
                     "ゃゅょっ"
                     "がぎぐげご"
                     "ざじずぜぞ"
                     "だぢづでど"
                     "ばびぶべぼ"
                     "ぱぴぷぺぽ"
                     " 　～〜]*$",
                   [unicode]),
    MP.


kana() ->
    {ok, MP} =
        re:compile("^[アイウエオ"
                     "カキクケコ"
                     "サシスセソ"
                     "タチツテト"
                     "ナニヌネノ"
                     "ハヒフヘホ"
                     "マミムメモ"
                     "ヤユヨ"
                     "ラリルレロ"
                     "ワヲン"
                     "ァィゥェォ"
                     "ャュョッ"
                     "ガギグゲゴ"
                     "ザジズゼゾ"
                     "ダヂヅデド"
                     "バビブベボ"
                     "パピプペポー"
                     "あいうえお"
                     "かきくけこ"
                     "さしすせそ"
                     "たちつてと"
                     "なにぬねの"
                     "はひふへほ"
                     "まみむめも"
                     "やゆよ"
                     "らりるれろ"
                     "わをん"
                     "ぁぃぅぇぉ"
                     "ゃゅょっ"
                     "がぎぐげご"
                     "ざじずぜぞ"
                     "だぢづでど"
                     "ばびぶべぼ"
                     "ぱぴぷぺぽ"
                     " 　～〜]*$",
                   [unicode]),
    MP.


latin() ->
    {ok, MP} = re:compile("^[a-zA-Z0-9/./,/-/?/:/; 　]*$", [unicode]),
    MP.


% FIXME: This doesn't cover native numbers at all, nor the various forms numbers
%        may take. Of course, covering all that is a non-trivial task...

numeric() ->
    {ok, MP} = re:compile("^[0-9０-９/,/，/./．]*$", [unicode]),
    MP.


% FIXME: kanji is currently "anything", which is close, but not quite true.

check_script("") ->
    empty;
check_script(String) ->
    case check_script(kana, String) of
        true ->
            kana;
        false ->
            case check_script(latin, String) of
                true  -> latin;
                false -> kanji
        end
    end.


check_script(kanji, _) ->
    true;
check_script(Script, String) ->
    MP = get_re(Script),
    case re:run(String, MP, [{capture, first, list}]) of
        {match, [String]} -> true;
        nomatch           -> false
    end.


get_re(hiragana) -> hiragana();
get_re(katakana) -> katakana();
get_re(kana)     -> kana();
get_re(latin)    -> latin();
get_re(numeric)  -> numeric().


-spec coerce(hiragana | katakana, unicode:chardata()) -> unicode:chardata().
%% NOTE: Naive, but less annoying to generate than looking up all the
%%       not-quite-sequential cases that occur in kana and full-width
%%       character "ranges".

coerce(hiragana, String) -> force_hiragana(String, []);
coerce(katakana, String) -> force_katakana(String, []);
coerce(ascii, String)    -> asciify(String, []).


force_hiragana([], O)       -> lists:reverse(O);
force_hiragana([$ア|T], O)  -> force_hiragana(T, [$あ|O]);
force_hiragana([$イ|T], O)  -> force_hiragana(T, [$い|O]);
force_hiragana([$ウ|T], O)  -> force_hiragana(T, [$う|O]);
force_hiragana([$エ|T], O)  -> force_hiragana(T, [$え|O]);
force_hiragana([$オ|T], O)  -> force_hiragana(T, [$お|O]);
force_hiragana([$カ|T], O)  -> force_hiragana(T, [$か|O]);
force_hiragana([$キ|T], O)  -> force_hiragana(T, [$き|O]);
force_hiragana([$ク|T], O)  -> force_hiragana(T, [$く|O]);
force_hiragana([$ケ|T], O)  -> force_hiragana(T, [$け|O]);
force_hiragana([$コ|T], O)  -> force_hiragana(T, [$こ|O]);
force_hiragana([$サ|T], O)  -> force_hiragana(T, [$さ|O]);
force_hiragana([$シ|T], O)  -> force_hiragana(T, [$し|O]);
force_hiragana([$ス|T], O)  -> force_hiragana(T, [$す|O]);
force_hiragana([$セ|T], O)  -> force_hiragana(T, [$せ|O]);
force_hiragana([$ソ|T], O)  -> force_hiragana(T, [$そ|O]);
force_hiragana([$タ|T], O)  -> force_hiragana(T, [$た|O]);
force_hiragana([$チ|T], O)  -> force_hiragana(T, [$ち|O]);
force_hiragana([$ツ|T], O)  -> force_hiragana(T, [$つ|O]);
force_hiragana([$テ|T], O)  -> force_hiragana(T, [$て|O]);
force_hiragana([$ト|T], O)  -> force_hiragana(T, [$と|O]);
force_hiragana([$ナ|T], O)  -> force_hiragana(T, [$な|O]);
force_hiragana([$ニ|T], O)  -> force_hiragana(T, [$に|O]);
force_hiragana([$ヌ|T], O)  -> force_hiragana(T, [$ぬ|O]);
force_hiragana([$ネ|T], O)  -> force_hiragana(T, [$ね|O]);
force_hiragana([$ノ|T], O)  -> force_hiragana(T, [$の|O]);
force_hiragana([$ハ|T], O)  -> force_hiragana(T, [$は|O]);
force_hiragana([$ヒ|T], O)  -> force_hiragana(T, [$ひ|O]);
force_hiragana([$フ|T], O)  -> force_hiragana(T, [$ふ|O]);
force_hiragana([$ヘ|T], O)  -> force_hiragana(T, [$へ|O]);
force_hiragana([$ホ|T], O)  -> force_hiragana(T, [$ほ|O]);
force_hiragana([$マ|T], O)  -> force_hiragana(T, [$ま|O]);
force_hiragana([$ミ|T], O)  -> force_hiragana(T, [$み|O]);
force_hiragana([$ム|T], O)  -> force_hiragana(T, [$む|O]);
force_hiragana([$メ|T], O)  -> force_hiragana(T, [$め|O]);
force_hiragana([$モ|T], O)  -> force_hiragana(T, [$も|O]);
force_hiragana([$ヤ|T], O)  -> force_hiragana(T, [$や|O]);
force_hiragana([$ユ|T], O)  -> force_hiragana(T, [$ゆ|O]);
force_hiragana([$ヨ|T], O)  -> force_hiragana(T, [$よ|O]);
force_hiragana([$ラ|T], O)  -> force_hiragana(T, [$ら|O]);
force_hiragana([$リ|T], O)  -> force_hiragana(T, [$り|O]);
force_hiragana([$ル|T], O)  -> force_hiragana(T, [$る|O]);
force_hiragana([$レ|T], O)  -> force_hiragana(T, [$れ|O]);
force_hiragana([$ロ|T], O)  -> force_hiragana(T, [$ろ|O]);
force_hiragana([$ワ|T], O)  -> force_hiragana(T, [$わ|O]);
force_hiragana([$ヲ|T], O)  -> force_hiragana(T, [$を|O]);
force_hiragana([$ン|T], O)  -> force_hiragana(T, [$ん|O]);
force_hiragana([$ァ|T], O)  -> force_hiragana(T, [$ぁ|O]);
force_hiragana([$ィ|T], O)  -> force_hiragana(T, [$ぃ|O]);
force_hiragana([$ゥ|T], O)  -> force_hiragana(T, [$ぅ|O]);
force_hiragana([$ェ|T], O)  -> force_hiragana(T, [$ぇ|O]);
force_hiragana([$ォ|T], O)  -> force_hiragana(T, [$ぉ|O]);
force_hiragana([$ガ|T], O)  -> force_hiragana(T, [$が|O]);
force_hiragana([$ギ|T], O)  -> force_hiragana(T, [$ぎ|O]);
force_hiragana([$グ|T], O)  -> force_hiragana(T, [$ぐ|O]);
force_hiragana([$ゲ|T], O)  -> force_hiragana(T, [$げ|O]);
force_hiragana([$ゴ|T], O)  -> force_hiragana(T, [$ご|O]);
force_hiragana([$ザ|T], O)  -> force_hiragana(T, [$ざ|O]);
force_hiragana([$ジ|T], O)  -> force_hiragana(T, [$じ|O]);
force_hiragana([$ズ|T], O)  -> force_hiragana(T, [$ず|O]);
force_hiragana([$ゼ|T], O)  -> force_hiragana(T, [$ぜ|O]);
force_hiragana([$ゾ|T], O)  -> force_hiragana(T, [$ぞ|O]);
force_hiragana([$ダ|T], O)  -> force_hiragana(T, [$だ|O]);
force_hiragana([$ヂ|T], O)  -> force_hiragana(T, [$ぢ|O]);
force_hiragana([$ヅ|T], O)  -> force_hiragana(T, [$づ|O]);
force_hiragana([$デ|T], O)  -> force_hiragana(T, [$で|O]);
force_hiragana([$ド|T], O)  -> force_hiragana(T, [$ど|O]);
force_hiragana([$バ|T], O)  -> force_hiragana(T, [$ば|O]);
force_hiragana([$ビ|T], O)  -> force_hiragana(T, [$び|O]);
force_hiragana([$ブ|T], O)  -> force_hiragana(T, [$ぶ|O]);
force_hiragana([$ベ|T], O)  -> force_hiragana(T, [$べ|O]);
force_hiragana([$ボ|T], O)  -> force_hiragana(T, [$ぼ|O]);
force_hiragana([$パ|T], O)  -> force_hiragana(T, [$ぱ|O]);
force_hiragana([$ピ|T], O)  -> force_hiragana(T, [$ぴ|O]);
force_hiragana([$プ|T], O)  -> force_hiragana(T, [$ぷ|O]);
force_hiragana([$ペ|T], O)  -> force_hiragana(T, [$ぺ|O]);
force_hiragana([$ポ|T], O)  -> force_hiragana(T, [$ぽ|O]);
force_hiragana([$ャ|T], O)  -> force_hiragana(T, [$ゃ|O]);
force_hiragana([$ュ|T], O)  -> force_hiragana(T, [$ゅ|O]);
force_hiragana([$ョ|T], O)  -> force_hiragana(T, [$ょ|O]);
force_hiragana([$ッ|T], O)  -> force_hiragana(T, [$っ|O]);
force_hiragana([H|T], O)    -> force_hiragana(T, [H|O]).


force_katakana([], O)       -> lists:reverse(O);
force_katakana([$あ|T], O)  -> force_katakana(T, [$ア|O]);
force_katakana([$い|T], O)  -> force_katakana(T, [$イ|O]);
force_katakana([$う|T], O)  -> force_katakana(T, [$ウ|O]);
force_katakana([$え|T], O)  -> force_katakana(T, [$エ|O]);
force_katakana([$お|T], O)  -> force_katakana(T, [$オ|O]);
force_katakana([$か|T], O)  -> force_katakana(T, [$カ|O]);
force_katakana([$き|T], O)  -> force_katakana(T, [$キ|O]);
force_katakana([$く|T], O)  -> force_katakana(T, [$ク|O]);
force_katakana([$け|T], O)  -> force_katakana(T, [$ケ|O]);
force_katakana([$こ|T], O)  -> force_katakana(T, [$コ|O]);
force_katakana([$さ|T], O)  -> force_katakana(T, [$サ|O]);
force_katakana([$し|T], O)  -> force_katakana(T, [$シ|O]);
force_katakana([$す|T], O)  -> force_katakana(T, [$ス|O]);
force_katakana([$せ|T], O)  -> force_katakana(T, [$セ|O]);
force_katakana([$そ|T], O)  -> force_katakana(T, [$ソ|O]);
force_katakana([$た|T], O)  -> force_katakana(T, [$タ|O]);
force_katakana([$ち|T], O)  -> force_katakana(T, [$チ|O]);
force_katakana([$つ|T], O)  -> force_katakana(T, [$ツ|O]);
force_katakana([$て|T], O)  -> force_katakana(T, [$テ|O]);
force_katakana([$と|T], O)  -> force_katakana(T, [$ト|O]);
force_katakana([$な|T], O)  -> force_katakana(T, [$ナ|O]);
force_katakana([$に|T], O)  -> force_katakana(T, [$ニ|O]);
force_katakana([$ぬ|T], O)  -> force_katakana(T, [$ヌ|O]);
force_katakana([$ね|T], O)  -> force_katakana(T, [$ネ|O]);
force_katakana([$の|T], O)  -> force_katakana(T, [$ノ|O]);
force_katakana([$は|T], O)  -> force_katakana(T, [$ハ|O]);
force_katakana([$ひ|T], O)  -> force_katakana(T, [$ヒ|O]);
force_katakana([$ふ|T], O)  -> force_katakana(T, [$フ|O]);
force_katakana([$へ|T], O)  -> force_katakana(T, [$ヘ|O]);
force_katakana([$ほ|T], O)  -> force_katakana(T, [$ホ|O]);
force_katakana([$ま|T], O)  -> force_katakana(T, [$マ|O]);
force_katakana([$み|T], O)  -> force_katakana(T, [$ミ|O]);
force_katakana([$む|T], O)  -> force_katakana(T, [$ム|O]);
force_katakana([$め|T], O)  -> force_katakana(T, [$メ|O]);
force_katakana([$も|T], O)  -> force_katakana(T, [$モ|O]);
force_katakana([$や|T], O)  -> force_katakana(T, [$ヤ|O]);
force_katakana([$ゆ|T], O)  -> force_katakana(T, [$ユ|O]);
force_katakana([$よ|T], O)  -> force_katakana(T, [$ヨ|O]);
force_katakana([$ら|T], O)  -> force_katakana(T, [$ラ|O]);
force_katakana([$り|T], O)  -> force_katakana(T, [$リ|O]);
force_katakana([$る|T], O)  -> force_katakana(T, [$ル|O]);
force_katakana([$れ|T], O)  -> force_katakana(T, [$レ|O]);
force_katakana([$ろ|T], O)  -> force_katakana(T, [$ロ|O]);
force_katakana([$わ|T], O)  -> force_katakana(T, [$ワ|O]);
force_katakana([$を|T], O)  -> force_katakana(T, [$ヲ|O]);
force_katakana([$ん|T], O)  -> force_katakana(T, [$ン|O]);
force_katakana([$ぁ|T], O)  -> force_katakana(T, [$ァ|O]);
force_katakana([$ぃ|T], O)  -> force_katakana(T, [$ィ|O]);
force_katakana([$ぅ|T], O)  -> force_katakana(T, [$ゥ|O]);
force_katakana([$ぇ|T], O)  -> force_katakana(T, [$ェ|O]);
force_katakana([$ぉ|T], O)  -> force_katakana(T, [$ォ|O]);
force_katakana([$が|T], O)  -> force_katakana(T, [$ガ|O]);
force_katakana([$ぎ|T], O)  -> force_katakana(T, [$ギ|O]);
force_katakana([$ぐ|T], O)  -> force_katakana(T, [$グ|O]);
force_katakana([$げ|T], O)  -> force_katakana(T, [$ゲ|O]);
force_katakana([$ご|T], O)  -> force_katakana(T, [$ゴ|O]);
force_katakana([$ざ|T], O)  -> force_katakana(T, [$ザ|O]);
force_katakana([$じ|T], O)  -> force_katakana(T, [$ジ|O]);
force_katakana([$ず|T], O)  -> force_katakana(T, [$ズ|O]);
force_katakana([$ぜ|T], O)  -> force_katakana(T, [$ゼ|O]);
force_katakana([$ぞ|T], O)  -> force_katakana(T, [$ゾ|O]);
force_katakana([$だ|T], O)  -> force_katakana(T, [$ダ|O]);
force_katakana([$ぢ|T], O)  -> force_katakana(T, [$ヂ|O]);
force_katakana([$づ|T], O)  -> force_katakana(T, [$ヅ|O]);
force_katakana([$で|T], O)  -> force_katakana(T, [$デ|O]);
force_katakana([$ど|T], O)  -> force_katakana(T, [$ド|O]);
force_katakana([$ば|T], O)  -> force_katakana(T, [$バ|O]);
force_katakana([$び|T], O)  -> force_katakana(T, [$ビ|O]);
force_katakana([$ぶ|T], O)  -> force_katakana(T, [$ブ|O]);
force_katakana([$べ|T], O)  -> force_katakana(T, [$ベ|O]);
force_katakana([$ぼ|T], O)  -> force_katakana(T, [$ボ|O]);
force_katakana([$ぱ|T], O)  -> force_katakana(T, [$パ|O]);
force_katakana([$ぴ|T], O)  -> force_katakana(T, [$ピ|O]);
force_katakana([$ぷ|T], O)  -> force_katakana(T, [$プ|O]);
force_katakana([$ぺ|T], O)  -> force_katakana(T, [$ペ|O]);
force_katakana([$ぽ|T], O)  -> force_katakana(T, [$ポ|O]);
force_katakana([$ゃ|T], O)  -> force_katakana(T, [$ャ|O]);
force_katakana([$ゅ|T], O)  -> force_katakana(T, [$ュ|O]);
force_katakana([$ょ|T], O)  -> force_katakana(T, [$ョ|O]);
force_katakana([$っ|T], O)  -> force_katakana(T, [$ッ|O]);
force_katakana([H|T], O)    -> force_katakana(T, [H|O]).


asciify([], O)      -> lists:reverse(O);
asciify([$０|T], O) -> asciify(T, [$0|O]);
asciify([$１|T], O) -> asciify(T, [$1|O]);
asciify([$２|T], O) -> asciify(T, [$2|O]);
asciify([$３|T], O) -> asciify(T, [$3|O]);
asciify([$４|T], O) -> asciify(T, [$4|O]);
asciify([$５|T], O) -> asciify(T, [$5|O]);
asciify([$６|T], O) -> asciify(T, [$6|O]);
asciify([$７|T], O) -> asciify(T, [$7|O]);
asciify([$８|T], O) -> asciify(T, [$8|O]);
asciify([$９|T], O) -> asciify(T, [$9|O]);
asciify([$ａ|T], O) -> asciify(T, [$a|O]);
asciify([$ｂ|T], O) -> asciify(T, [$b|O]);
asciify([$ｃ|T], O) -> asciify(T, [$c|O]);
asciify([$ｄ|T], O) -> asciify(T, [$d|O]);
asciify([$ｅ|T], O) -> asciify(T, [$e|O]);
asciify([$ｆ|T], O) -> asciify(T, [$f|O]);
asciify([$ｇ|T], O) -> asciify(T, [$g|O]);
asciify([$ｈ|T], O) -> asciify(T, [$h|O]);
asciify([$ｉ|T], O) -> asciify(T, [$i|O]);
asciify([$ｊ|T], O) -> asciify(T, [$j|O]);
asciify([$ｋ|T], O) -> asciify(T, [$k|O]);
asciify([$ｌ|T], O) -> asciify(T, [$l|O]);
asciify([$ｍ|T], O) -> asciify(T, [$m|O]);
asciify([$ｎ|T], O) -> asciify(T, [$n|O]);
asciify([$ｏ|T], O) -> asciify(T, [$o|O]);
asciify([$ｐ|T], O) -> asciify(T, [$p|O]);
asciify([$ｑ|T], O) -> asciify(T, [$q|O]);
asciify([$ｒ|T], O) -> asciify(T, [$r|O]);
asciify([$ｓ|T], O) -> asciify(T, [$s|O]);
asciify([$ｔ|T], O) -> asciify(T, [$t|O]);
asciify([$ｕ|T], O) -> asciify(T, [$u|O]);
asciify([$ｖ|T], O) -> asciify(T, [$v|O]);
asciify([$ｗ|T], O) -> asciify(T, [$w|O]);
asciify([$ｘ|T], O) -> asciify(T, [$x|O]);
asciify([$ｙ|T], O) -> asciify(T, [$y|O]);
asciify([$ｚ|T], O) -> asciify(T, [$z|O]);
asciify([$Ａ|T], O) -> asciify(T, [$A|O]);
asciify([$Ｂ|T], O) -> asciify(T, [$B|O]);
asciify([$Ｃ|T], O) -> asciify(T, [$C|O]);
asciify([$Ｄ|T], O) -> asciify(T, [$D|O]);
asciify([$Ｅ|T], O) -> asciify(T, [$E|O]);
asciify([$Ｆ|T], O) -> asciify(T, [$F|O]);
asciify([$Ｇ|T], O) -> asciify(T, [$G|O]);
asciify([$Ｈ|T], O) -> asciify(T, [$H|O]);
asciify([$Ｉ|T], O) -> asciify(T, [$I|O]);
asciify([$Ｊ|T], O) -> asciify(T, [$J|O]);
asciify([$Ｋ|T], O) -> asciify(T, [$K|O]);
asciify([$Ｌ|T], O) -> asciify(T, [$L|O]);
asciify([$Ｍ|T], O) -> asciify(T, [$M|O]);
asciify([$Ｎ|T], O) -> asciify(T, [$N|O]);
asciify([$Ｏ|T], O) -> asciify(T, [$O|O]);
asciify([$Ｐ|T], O) -> asciify(T, [$P|O]);
asciify([$Ｑ|T], O) -> asciify(T, [$Q|O]);
asciify([$Ｒ|T], O) -> asciify(T, [$R|O]);
asciify([$Ｓ|T], O) -> asciify(T, [$S|O]);
asciify([$Ｔ|T], O) -> asciify(T, [$T|O]);
asciify([$Ｕ|T], O) -> asciify(T, [$U|O]);
asciify([$Ｖ|T], O) -> asciify(T, [$V|O]);
asciify([$Ｗ|T], O) -> asciify(T, [$W|O]);
asciify([$Ｘ|T], O) -> asciify(T, [$X|O]);
asciify([$Ｙ|T], O) -> asciify(T, [$Y|O]);
asciify([$Ｚ|T], O) -> asciify(T, [$Z|O]);
asciify([H|T], O)   -> asciify(T, [H|O]).
